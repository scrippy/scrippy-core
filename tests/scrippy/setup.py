import os
import yaml

conf_file = "/etc/scrippy/scrippy.yml"
with open(conf_file, mode="r", encoding="utf-8") as conf:
  scrippy_conf = yaml.load(conf, Loader=yaml.FullLoader)
  for rep in scrippy_conf["env"]:
    os.makedirs(scrippy_conf["env"][rep], 0o0755)
