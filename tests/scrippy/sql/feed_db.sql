DROP TABLE IF EXISTS users;
CREATE TABLE users(
  id          SERIAL PRIMARY KEY NOT NULL,
  name        TEXT               NOT NULL,
  givenname   TEXT               NOT NULL,
  password    TEXT               NOT NULL
);

INSERT INTO users values (0, 'FINK', 'Harry', 'D34D P4RR0T');
