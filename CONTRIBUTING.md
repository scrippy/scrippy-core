##  Environnement de dev

```sh
python3 -m venv scrippy-core
source scrippy-core/bin/activate
pip install -e .
```

## Pylint
### Changement par rapport à la norme :
[Nombre d'espaces] (.pylintrc#L449) Réduction à 2 espaces au lieu de 4

[Chaîne de caractères utilisée pour les tabulations] (.pylintrc#L453) Réduction à 2 espaces conformément à la ligne ci-dessus

[Taille d'une ligne] (.pylintrc#L456) Augmentation de la taille de ligne autorisée à 200 au lieu de 100 (Plus personne n'imprime de code !)

[Interpolation des logs] (.pylintrc#L242) Désactivation du check de l'interpolation pour le module logging

[Import inutiles dans les __init__] (.pylintrc#L434) Recherche activée des imports inutiles dans les fichiers __init__


# Conventions

## Commits

Les messages de commits doivent respecter la spécification [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

```
<type>[(optional scope)]: <description>
```

### Types

**New features**
* feat

**Fixes**
* fix

**Refactorings**
* refactor

**Others**
* build
* chore
* ci
* docs
* style
* perf
* test

### Scopes
Les scopes correspondent aux noms des modules jusqu'au deuxième niveau.

**Sans caratères speciaux !**

**exemple :** `scrippy_remote.remote.ftp`, `scrippy_remote.remote.ssh`, `scrippy_remote.remote`, `scrippy_db.db`, `scrippy_mail.mail.mailer`, etc...

### Breaking change
Pour les breaking changes ajouter `!` avant le `: <description>`

**Exemple :**
```
refactor!: drop support for Python3.6
```

Il est aussi possible d'ajouter les détails de cette façon :
```
refactor!: drop support for Python3.6

BREAKING CHANGE: refactor to use Python features not available in Python3.6
```

## Lancement des tests
pytest tests -v
