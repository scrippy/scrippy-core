#!/bin/env python3
"""
--------------------------------------------------------------------------------
  @author         : MCO-System
  @date           : 2023-08-04
  @version        : 1.0.0
  @description    : Sample Script
--------------------------------------------------------------------------------
  Changelog:
    1.0.0  2023-08-04 - MCO-System - Cre: Creation
--------------------------------------------------------------------------------
  Allowed users and groups:
    @user:mco
    @group:mco
--------------------------------------------------------------------------------
  Concurrent executions:
    @max_instance: 2
    @timeout: 30
    @exit_on_wait: false
    @exit_on_timeout: true
--------------------------------------------------------------------------------
  Script Configuration:
    #  @conf:section|var|type|secret
    @conf:files|lock|str|false
    @conf:files|exclude|str|false
--------------------------------------------------------------------------------
  Options and parameters:
    #  @args:name|type|help|num_args|default|conflicts|implies|required|secret
    @args:dir|str|Directory to list|1||||true|false
    @args:recursive|bool|Copy recursively|1|false|||false|false
    @args:countdown|int|Countdown timer value|1||||false|false

--------------------------------------------------------------------------------
  Detail:
  ---------------
    This dummy sample script list all files in the directory specified by the `--dir` option.

    If a value is passed to the `--countdown` option, it will be used as a countdown timer after the file copy.

    Up to 2 instances of this script can be run simultaneously. All instances over the second one will be paused until one of the currently running instance ends.

    While perfectly functional, this script is a demonstration script that has limited use case.

    Configuration:
      - files::lock = List files only if specified file (full path) is present.
      - files::exclude = File matching exclude pattern (glob pattern) will not be listed.

"""
# ------------------------------------------------------------------------------
#  Environment Initialization
# ------------------------------------------------------------------------------
import os
import time
import glob
import scrippy_core
from scrippy_core import ScrippyCoreError, logger


# ------------------------------------------------------------------------------
#  Classes and functions
# ------------------------------------------------------------------------------
def list_files(path, exclude, recursive=False):
  logger.info(f"[+] List {path} excluding {exclude} [recursive={recursive}]")
  if os.path.isdir(path):
    files = glob.glob(f"{os.path.join(path, '*')}",
                      recursive=recursive)
    for index, fname in enumerate(files):
      if recursive and os.path.isdir(fname):
        list_files(path=fname,
                   exclude=exclude,
                   recursive=recursive)
      logger.info(f" '-> {fname}")


def countdown(delay):
  logger.info("[+] Starting countdown:")
  while delay > 0:
    logger.info(f" '-> Still waiting for: {delay}s")
    time.sleep(1)
    delay -= 1


# ------------------------------------------------------------------------------
#  Main
# ------------------------------------------------------------------------------
def main():
  with scrippy_core.ScriptContext() as _context:
    config = _context.config
    args = _context.args
    if os.path.isfile(config.get("files", "lock")):
      if os.path.isdir(args.dir):
        list_files(path=args.dir,
                   exclude=config.get("files", "exclude"),
                   recursive=args.recursive)
      else:
        raise ScrippyCoreError(f"No such directory: {args.dir}")
    else:
      logger.info(f"[+] Mandatory file {config.get('files', 'lock')} is not present: Aborting.")
    if args.countdown is not None and args.countdown > 0:
      countdown(delay=args.countdown)


# ------------------------------------------------------------------------------
#  Entry point
# ------------------------------------------------------------------------------
if __name__ == '__main__':
  main()
