scrippy\_core.logger package
============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   scrippy_core.logger.manager

Module contents
---------------

.. automodule:: scrippy_core.logger
   :members:
   :undoc-members:
   :show-inheritance:
