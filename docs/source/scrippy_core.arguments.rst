scrippy\_core.arguments package
===============================

Submodules
----------

scrippy\_core.arguments.historyaction module
--------------------------------------------

.. automodule:: scrippy_core.arguments.historyaction
   :members:
   :undoc-members:
   :show-inheritance:

scrippy\_core.arguments.logaction module
----------------------------------------

.. automodule:: scrippy_core.arguments.logaction
   :members:
   :undoc-members:
   :show-inheritance:

scrippy\_core.arguments.sourceaction module
-------------------------------------------

.. automodule:: scrippy_core.arguments.sourceaction
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: scrippy_core.arguments
   :members:
   :undoc-members:
   :show-inheritance:
