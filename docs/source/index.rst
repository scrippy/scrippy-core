.. Scrippy Core documentation master file, created by
   sphinx-quickstart on Sat Dec 31 11:55:46 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Scrippy Core's documentation!
========================================

.. toctree::
   :maxdepth: 10
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
